#include "Keitai.h"

#include <stdio.h>

#include <WinSock2.h>

#include <WS2tcpip.h>

void recieve(char* buf, int len) {
	printf("%d: %s\n", len, buf);
}

int main() {
	WSADATA wsaData;
	SOCKET sock;
	char buf[64];
	struct sockaddr_in server;

	WSAStartup(MAKEWORD(2, 0), &wsaData);
	sock = socket(AF_INET, SOCK_STREAM, 0);
	server.sin_family = AF_INET;
	server.sin_port = htons(9999);
	inet_pton(AF_INET, "127.0.0.1", &((struct sockaddr_in *)&server)->sin_addr);

	connect(sock, (struct sockaddr *)&server, sizeof(server));
	printf("connected\n");

	while (true) {
		memset(buf, 0, sizeof(buf));
		int n = recv(sock, buf, sizeof(buf), 0);
		recieve(buf, n);
	}

	WSACleanup();
	return 0;
}