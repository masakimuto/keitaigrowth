package com.kylab.keitaigrowth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import android.os.Handler;

import java.util.logging.LogRecord;

/**
 * Created by muto on 2015/09/22.
 */
public abstract class MessageListener {
    private  final int port;
    private boolean connected = false;
    Socket socket;
    Handler handler;

    //private  final Handler handler;
    private  final  Runnable acceptTask = new Runnable() {
        @Override
        public void run() {
            final ServerSocket serverSocket;
            try{
                serverSocket = new ServerSocket(port);
            }
            catch (IOException e){
                throw new RuntimeException(e);
            }
            try {
                socket = serverSocket.accept();
                connected = true;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        onConnected();
                    }
                });
            }
            catch (IOException e){
                throw  new RuntimeException(e);
            }
            finally {
                try{
                    serverSocket.close();
                }
                catch (IOException e){
                    throw  new RuntimeException(e);
                }
            }
        }
    };

    public  MessageListener(int port){
        this.port = port;
        handler = new Handler();
        Executors.newSingleThreadExecutor().execute(acceptTask);
    }

    public  boolean isConnected(){
        return  connected;
    }

    public void send(String str){
        try{
            socket.getOutputStream().write(str.getBytes());

        }
        catch (IOException e){
            throw  new RuntimeException(e);
        }
    }

    protected abstract void onConnected();

    public void destroy(){
        if(socket != null){
            try{
                socket.close();

            }
            catch (IOException e){

            }
            finally {
                socket = null;
            }

        }
    }
}
