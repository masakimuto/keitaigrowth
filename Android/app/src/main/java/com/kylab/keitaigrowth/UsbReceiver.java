package com.kylab.keitaigrowth;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.util.Log;

/**
 * Created by muto on 2015/09/22.
 */
public class UsbReceiver extends BroadcastReceiver {

    public interface Driver{
        public  void openAccessory(UsbAccessory accessory);
        public void closeAccessory(UsbAccessory accessory);
    }

    static  final  String TAG="USBReceiver";

    Driver driver;
    UsbManager manager;
    UsbAccessory accessory;
    PendingIntent intent;
    final String permission;
    boolean pending;
    Activity activity;

    public UsbReceiver(Activity activity, String permission, Driver driver){
        super();
        this.driver = driver;
        this.activity = activity;
        this.permission = permission;
        this.manager = (UsbManager)activity.getSystemService(Context.USB_SERVICE);
        intent = PendingIntent.getBroadcast(activity, 0, new Intent(permission), 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(permission);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        activity.registerReceiver(this, filter);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(permission.equals(action)){
            open(intent);
        } else if(UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)){
            close(intent);
        }
    }

    private  synchronized void open(Intent intent){
        UsbAccessory accessory = (UsbAccessory)intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
        if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)){
            driver.openAccessory(accessory);
            this.accessory = accessory;
        }else{
            Log.d(TAG, "permission denied " + accessory);
        }
        pending = false;
    }

    private synchronized void close(Intent intent){
        UsbAccessory accessory = (UsbAccessory)intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
        if(accessory != null && accessory.equals(this.accessory)){
            close();
        }
    }

    public  synchronized void close(){
        if(this.accessory != null){
            driver.closeAccessory(accessory);
            accessory = null;
        }
    }

    public void resume(){
        UsbAccessory[] accessories = manager.getAccessoryList();
        UsbAccessory accessory = accessories == null ? null : accessories[0];
        if(accessory != null){
            if(manager.hasPermission(accessory)){
                driver.openAccessory(accessory);
            }else{
                synchronized (this){
                    if(!pending){
                        manager.requestPermission(accessory, intent);
                        pending = true;
                    }
                }
            }
        }else {
            Log.d(TAG, "accessory is null");
        }
    }

    public void destroy(){
        activity.unregisterReceiver(this);

    }
}
