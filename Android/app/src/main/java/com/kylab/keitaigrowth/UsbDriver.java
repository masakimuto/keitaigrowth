package com.kylab.keitaigrowth;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by muto on 2015/09/22.
 */
public class UsbDriver implements UsbReceiver.Driver {

    static  final  String TAG="USBDriver";
    FileOutputStream out;
    ParcelFileDescriptor descriptor;
    UsbManager manager;

    public  UsbDriver(Activity activity){
        manager = (UsbManager)activity.getSystemService(Context.USB_SERVICE);
    }

    @Override
    public void openAccessory(UsbAccessory accessory) {
        descriptor = manager.openAccessory(accessory);
        if(descriptor != null){
            FileDescriptor fd = descriptor.getFileDescriptor();
            out = new FileOutputStream(fd);
            Log.d(TAG, "accessory open");
        }else{
            Log.d(TAG, "accessory open fail");
        }
    }

    @Override
    public void closeAccessory(UsbAccessory accessory) {
        try
        {
            if(descriptor != null){
                descriptor.close();
            }

        }
        catch (IOException e){

        }
        finally {
            descriptor = null;
        }
    }

    public  void  send(String msg) throws IOException{
        out.write(msg.getBytes("UTF-8"));
        out.flush();
    }
}
