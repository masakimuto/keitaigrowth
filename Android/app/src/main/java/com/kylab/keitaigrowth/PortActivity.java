package com.kylab.keitaigrowth;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by muto on 2015/09/22.
 */
public class PortActivity  extends Activity{

    MessageListener listener;
    PhoneListener phoneListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listener =  new MessageListener(8080){
            @Override
            protected void onConnected() {
                ((TextView)findViewById(R.id.status)).setText("ready");
            }
        };
        phoneListener = new PhoneListener(this) {
            @Override
            protected void onCall(String number) {
                listener.send(number);
            }
        };
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View v){
        if(listener.isConnected()){
            EditText edit = (EditText)findViewById(R.id.edit);
            listener.send(edit.getText().toString());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listener.destroy();
    }
}
