package com.kylab.keitaigrowth;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

/**
 * Created by muto on 2015/09/22.
 */
public abstract class PhoneListener {

    Handler handler;

    public PhoneListener(Activity activity){
        handler = new Handler();
        TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        PhoneStateListener listener = new PhoneStateListener(){
            @Override
            public void onCallStateChanged(int state, final String incomingNumber) {
                switch (state){
                    case TelephonyManager.CALL_STATE_RINGING:
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                onCall(incomingNumber);

                            }
                        });
                        break;
                    default:
                        break;

                }
            }
        };
        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    protected abstract void onCall(String number);
}
