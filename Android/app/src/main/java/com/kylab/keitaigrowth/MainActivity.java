package com.kylab.keitaigrowth;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Debug;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MainActivity extends Activity {

    //UsbAccessory accessory;
    //UsbManager manager;

    TelephonyManager telephonyManager;

    UsbReceiver receiver;
    UsbDriver driver;

    static  final  String PERMISSION = "kylab.keitai.usb";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        telephonyManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        PhoneStateListener listener = new PhoneStateListener(){
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state){
                    case TelephonyManager.CALL_STATE_RINGING:
                        Log.d("phone", incomingNumber);
                        break;
                    default:
                        break;
                }
            }
        };
        driver = new UsbDriver(this);
        receiver = new UsbReceiver(this, PERMISSION, driver);


        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        receiver.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        receiver.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        receiver.destroy();
    }



    public void onButtonClick(View sender){
        Toast.makeText(this, "hoge", Toast.LENGTH_SHORT).show();
        try{
            driver.send("hoge");
        }
        catch (IOException e){
            Log.d("Main", e.toString());
        }
    }

}
